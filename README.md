# Visualiseur des données de la CoopTech

Widget **[Datami](https://gitlab.com/multi-coop/datami)** de prévisualisation des données de la CoopTech.

---

## Résumé

La CoppTech cherche à constituer un annuaire le plus complet possible des SCOP et autres coopératives du numérique en France. Cet annuaire prend la forme d'un simple fichier `.csv` hébergé sur un repo [Github](https://github.com/multi-coop/gitribute-content-test/blob/main/data/csv/cooptech/Annuaire-SCOP-SCIC-tech-France.csv) ou [Gitlab](https://gitlab.com/multi-coop/gitribute-content-test/-/blob/main/data/csv/cooptech/Annuaire-SCOP-SCIC-tech-France.csv)

Afin de pouvoir partager et de mettre en valeur toutes ces ressources il a été proposé d'utiliser le _widget open source_ créé par la coopérative numérique **[multi](https://multi.coop)** : le projet **[Datami](https://gitlab.com/multi-coop/datami)**.

Cet outil permet la **visualisation** des données mais également la **contribution**.

**Datami** permet d'intégrer sur des sites tiers (sites de partenaires ou autres) une sélection plus ou moins large de ressources. Cette solution permet à la fois d'éviter aux sites partenaires de "copier-coller" les ressources, d'afficher sur ces sites tiers les ressources toujours à jour, et de permettre aux sites tiers ainsi qu'au site source de gagner en visibilité, en légitimité et en qualité d'information.

L'autre avantage de cette solution est qu'elle n'est déployée qu'une fois, mais que le widget peut être intégré et paramétré/personnalisé sur autant de sites tiers que l'on souhaite... **gratuitement**.

---

## Démo

[![Netlify Status](https://api.netlify.com/api/v1/badges/57d96b44-b6c4-4f0a-a7f7-412571f23aa2/deploy-status)](https://app.netlify.com/sites/datami-demo-cooptech/deploys)
- URL de démo : 
  - DEMO / données CoopTech : https://datami-demo-gitribute-cooptech.netlify.app/

---

### Documentation 

Un site dédié à la doucmentation technique de Datami est consultable ici : https://datami-docs.multi.coop

---

## Crédits

| | logo | url |
| :-: | :-: | :-: |
| **CoopTech** | ![ODF](./images/cooptech_logo.svg) | https://www.cooptech.fr/ |
| **coopérative numérique multi** | ![multi](./images/multi-logo.png) | https://multi.coop |

---

## Pour aller plus loin...

### Datami

Le widget fait partie intégrante du projet [Datami](https://gitlab.com/multi-coop/datami)
